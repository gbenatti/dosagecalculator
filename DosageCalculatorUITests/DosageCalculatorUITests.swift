//
//  DosageCalculatorUITests.swift
//  DosageCalculatorUITests
//
//  Created by Georges Benatti Jr on 6/21/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import XCTest

class DosageCalculatorUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNavigationToList() {
        
        let app = XCUIApplication()
        app.tables.staticTexts["Amoxicilina 50mg/mL, Pó para suspensão oral (1un de 150mL)"].tap()
        app.navigationBars["Selecione uma posologia"].buttons[" "].tap()
    }
}
