//
//  DosageCalculatorViewModelTests.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/22/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import XCTest

class DosageCalculatorViewModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testViewModelShouldBeCreatedWithCorrectDosageResult() {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency8by8,
                                           lengthRange: 10..<21)
        
        XCTAssertEqual(666, vm.dosage)
    }
    
    func testChangingFrequencyShouldRecalculateDosage()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency8by8,
                                           lengthRange: 10..<21)
        
        vm.frequency = .frequency6by6
        
        XCTAssertEqual(500, vm.dosage)
    }
    
    func testChangingWeightShouldRecalculateDosage()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)
        
        vm.weight = 50
        
        XCTAssertEqual(625, vm.dosage)
    }
    
    func testChangingConcentrationShouldRecalculateDosage()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)

        vm.concentration = 40
        
        XCTAssertEqual(1600, vm.dosage)
    }
    
    // keep inside ranges
    
    func testSettingConcentrationShouldStayInsideRangeUpperBound()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)
        
        vm.concentration = 100
        
        XCTAssertEqual(50, vm.concentration)
        XCTAssertEqual(2000, vm.dosage)
    }
    
    func testSettingConcentrationShouldStayInsideRangeLowerBound()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)
        
        vm.concentration = 5
        
        XCTAssertEqual(25, vm.concentration)
        XCTAssertEqual(1000, vm.dosage)
    }

    func testSettingLengthShouldStayInsideRangeUpperBound()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)
        
        vm.length = 100
        
        XCTAssertEqual(20, vm.length)
    }
    
    func testSettingLengthShouldStayInsideRangeLowerBound()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)
        
        vm.length = 5
        
        XCTAssertEqual(10, vm.length)
    }

    // update ui
    
    func testCallingUpdateAfterChangingDosageShouldInvokeCallback()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)

        var changeCount = 0
        vm.dosageDidChange = {dosage in changeCount += 1}
        
        XCTAssertEqual(0, changeCount)
        
        vm.frequency = .frequency6by6
        vm.update()
        XCTAssertEqual(1, changeCount)
        
        vm.weight = 90
        vm.update()
        XCTAssertEqual(2, changeCount)
        
        vm.frequency = .frequency6by6
        vm.update()
        XCTAssertEqual(2, changeCount)
    }
    
    func testCallingForcedUpdateShouldInvokeCallback()
    {
        let vm = DosageCalculatorViewModel(substance: "Amoxilina",
                                           weight: 80,
                                           concentrationRange: 25..<51,
                                           frequency: .frequency12by12,
                                           lengthRange: 10..<21)
        
        var changeCount = 0
        vm.dosageDidChange = {dosage in changeCount += 1}
        
        XCTAssertEqual(0, changeCount)
        
        vm.update()
        XCTAssertEqual(0, changeCount)

        vm.update(true)
        XCTAssertEqual(1, changeCount)
    }
}
