//
//  String+Regex.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/24/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

extension String {
    func matches(for regex: String!) -> [String] {
        
        do {
            let regex = try RegularExpression(pattern: regex, options: [.caseInsensitive])
            
            let nsString = self as NSString
            let matches = regex.matches(in: self, range: NSMakeRange(0, nsString.length))
            var result = [String]()
            
            for match in matches as [TextCheckingResult] {
                for i in 1..<match.numberOfRanges {
                    let substring = nsString.substring(with: match.range(at: i))
                    result.append(substring)
                }
            }
            
            return result
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }

}
