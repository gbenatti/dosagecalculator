//
//  PosologyCell.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/24/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation
import UIKit

class PosologyCell: UITableViewCell {
    static let reuseIdentifier = "posologyCell"
    
    var singlePosology: Bool = false
    var dosage: Int = 0
    var length: Int = 0
    var frequency: DosageFrequency!
    var presentation: Presentation?
    var marked: Bool = false
    
    @IBOutlet weak var descriplionLabel: UILabel!
    @IBOutlet weak var prescriptionLabel: UILabel!
    
    func setModel(singlePosology: Bool, dosage: Int, length: Int, frequency: DosageFrequency, presentation: Presentation, marked: Bool) {
        self.singlePosology = singlePosology
        self.dosage = dosage
        self.length = length
        self.frequency = frequency
        self.presentation = presentation
        self.marked = marked
        
        descriplionLabel.text = presentation.descriptionText
        prescriptionLabel.text = presentation.prescriptionText(
            with: dosage,
            length: length,
            frequency: frequency)
        prescriptionLabel.numberOfLines = 0
        prescriptionLabel.sizeToFit()
        
        accessoryType = singlePosology ? .disclosureIndicator : (marked ? .checkmark : .none)
    }
}
