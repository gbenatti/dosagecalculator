//
//  DosageCalculatorViewModel.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/21/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

class DosageCalculatorViewModel {
    
    let substance: String
    var weight: Int {
        didSet(newValue) {
            update()
        }
    }
    
    let concentrationRange: CountableRange<Int>
    
    var frequency: DosageFrequency {
        didSet(newValue) {
            update()
        }
    }
    
    let lengthRange: CountableRange<Int>
    
    var concentration: Int {
        set(newConcentration) {
            _concentration = newConcentration.clamp(range: concentrationRange)
            update()
        }
        get {
            return _concentration
        }
    }
    
    private var _concentration: Int = 0

    var length: Int {
        set(newLength) {
            _length = newLength.clamp(range: lengthRange)
            updateUI()
        }
        get {
            return _length
        }
    }
    
    private var _length: Int = 0
    
    var dosage: Int {
        return _dosage
    }
    
    private var _dosage: Int = 0
    
    static var initial: DosageCalculatorViewModel {
        return DosageCalculatorViewModel(substance: "Amoxilina",
                                         weight: 80,
                                         concentrationRange: 25..<51,
                                         frequency: .frequency8by8,
                                         lengthRange: 10..<21
        )
    }
    
    var dosageDidChange: ((model: DosageCalculatorViewModel) -> Void)?

    init(substance: String, weight: Int, concentrationRange: CountableRange<Int>, frequency: DosageFrequency, lengthRange: CountableRange<Int>) {
        self.substance = substance
        self.weight = weight
        self.concentrationRange = concentrationRange
        self.frequency = frequency
        self.lengthRange = lengthRange
        self.length = lengthRange.lowerBound
        self.concentration = concentrationRange.lowerBound
    }
    
    func update(_ force: Bool = false) {
        if recalculate() || force {
            updateUI()
        }
    }
    
    private func recalculate() -> Bool {
        let calculator = DosageCalculator(concentration: concentration,
                                          weight: weight,
                                          frequency: frequency)
        
        let newDosage = calculator.result
        
        if newDosage != _dosage {
            _dosage = newDosage
            return true
        }
        
        return false
    }
    
    private func updateUI() {
        if let dosageDidChange = dosageDidChange {
            dosageDidChange(model: self)
        }
    }
}
