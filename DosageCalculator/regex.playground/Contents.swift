//: Playground - noun: a place where people can play

import UIKit

var str = "Amoxicilina 125mg/5mL"

//(.*) ([0-9]*)mg - Amoxicilina 500mg
//(.*) ([0-9]*)mg\/mL - Amoxicilina 50mg/mL
//(.*) ([0-9]*)mg\/(.*)mL - Amoxicilina 125mg/5mL

func matches(for regex: String!, in text: String!) -> [String] {
    
    do {
        let regex = try RegularExpression(pattern: regex, options: [.caseInsensitive])
        
        let nsString = text as NSString
        let matches = regex.matches(in: text, range: NSMakeRange(0, nsString.length))
        var result = [String]()
        
        for match in matches as [TextCheckingResult] {
            for i in 1..<match.numberOfRanges {
                let substring = nsString.substring(with: match.range(at: i))
                result.append(substring)
            }
        }
        
        return result
        
    } catch let error as NSError {
        print("invalid regex: \(error.localizedDescription)")
        return []
    }
}

for m in matches(for: "(.*) ([0-9]*)mg/(.*)mL$", in: str) {
    print(m)
}
