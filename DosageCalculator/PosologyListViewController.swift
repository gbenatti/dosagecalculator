//
//  PosologyListViewController.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/24/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation
import UIKit

class PosologyListViewController: UITableViewController
{
    var singlePosology: Bool = false
    var dosage: Int = 0
    var length: Int = 0
    var frequency: DosageFrequency!
    var presentations: [Presentation] = []
    var actual: Presentation!
    
    var substance: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setModel(singlePosology: Bool, substance: String, dosage: Int, length: Int, frequency: DosageFrequency, actual: Presentation, presentations: [Presentation]) {
        self.singlePosology = singlePosology
        self.dosage = dosage
        self.length = length
        self.frequency = frequency
        self.presentations = presentations
        self.actual = actual
        self.substance = substance
        
        self.tableView.reloadData()
    }
}

extension PosologyListViewController // UITable specific stuff
{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let presentation = presentations[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: PosologyCell.reuseIdentifier, for: indexPath) as! PosologyCell
        cell.setModel(
            singlePosology: singlePosology,
            dosage: dosage,
            length: length,
            frequency: frequency,
            presentation: presentation,
            marked: !singlePosology && presentation.ean == actual.ean)
        
        return cell
    }
}
