//
//  DosageFrequency.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/23/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

enum DosageFrequency {
    case frequency6by6
    case frequency8by8
    case frequency12by12
    
    var textRepresentation: String {
        switch self {
        case .frequency6by6:
            return "6/6"
        case .frequency8by8:
            return "8/8"
        case .frequency12by12:
            return "12/12"
        }
    }
    
    var times: Int {
        switch self {
        case .frequency6by6:
            return 4;
        case .frequency8by8:
            return 3;
        case .frequency12by12:
            return 2;
        }
    }
    
    var rangeText: String {
        switch self {
        case .frequency6by6:
            return "6 em 6 horas";
        case .frequency8by8:
            return "8 em 8 horas";
        case .frequency12by12:
            return "12 em 12 horas";
        }
    }
    
    var index: Int
    {
        switch self {
        case .frequency6by6:
            return 0
        case .frequency8by8:
            return 1
        case .frequency12by12:
            return 2
        }
    }
    
    static func frequencyBy(index: Int) -> DosageFrequency? {
        switch index {
        case 0:
            return .frequency6by6
        case 1:
            return .frequency8by8
        case 2:
            return .frequency12by12
        default:
            return nil
        }
    }
}
