//
//  PresentationRepository.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/23/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

class PresentationRepository
{
    lazy var presentations: [Presentation] = {
        return self.loadPresentations()
    }()
    
    func findBy(dosage: Int, substance: String) -> Presentation? {
        var ordered = presentations.map {
            Presentation(ean: $0.ean, title: $0.title, subtitle: $0.subtitle, description: $0.description)
        }
        
        ordered.sort(isOrderedBefore: {p1, p2 in
            if let p1type = p1.type, let p2type = p2.type {
                return PresentationType.bestPresentatiomForDosage(p1: p1type, p2: p2type, dosage: dosage)
            }
            
            return p2.type != nil ? false : true
        })
        
        return ordered.first
    }
    
    private func loadPresentations() -> [Presentation] {
        var result = [Presentation]()
        
        if let jsonTxt = readMockData() {
            if let json = parseJson(jsonText: jsonTxt) {
                for data in json["data"] as! [AnyObject] {
                    let type = data["type"] as! String
                    if type == "apresentacoes" {
                        let presentation = parsePresentation(json: data)
                        if presentation.type != nil {
                            result.append(presentation)
                        }
                    }
                }
            }
        }

        return result
    }
    
    private func readMockData() -> String? {
        if let destinationPath = Bundle.main.pathForResource("mock", ofType: "json") {
            let filemgr = FileManager.default
            if filemgr.fileExists(atPath: destinationPath) {
                do {
                    let readFile = try String(contentsOfFile: destinationPath, encoding: String.Encoding.utf8)
                    return readFile
                } catch let error as NSError {
                    print("Error: \(error)")
                }
            }
        }
        
        return nil
    }
    
    private func parseJson(jsonText: String) -> [String: AnyObject]? {
        do {
            if let data = jsonText.data(using: String.Encoding.utf8) {
                let json = try JSONSerialization.jsonObject(with: data) as! [String: AnyObject]
                return json
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    private func parsePresentation(json: AnyObject) -> Presentation {
        let dict = json["attributes"] as! [String: AnyObject]
        
        let ean = dict["ean"] as! String
        let title = dict["titulo"] as! String
        let subtitle = dict["subtitulo"] as! String
        let description = dict["descricao"] as! String
        
        return Presentation(ean: ean,
                            title: title,
                            subtitle: subtitle,
                            description: description)
    }
}
