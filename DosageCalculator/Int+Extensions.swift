//
//  Int+Extensions.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/26/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

extension Int
{
    func clamp(range: CountableRange<Int>) -> Int {
        guard self >= range.lowerBound else { return range.lowerBound }
        guard self < range.upperBound else { return range.upperBound - 1 }
        
        return self
    }
    
    func clamp(range: CountableClosedRange<Int>) -> Int {
        guard self >= range.lowerBound else { return range.lowerBound }
        guard self <= range.upperBound else { return range.upperBound }
        
        return self
    }
    
}
