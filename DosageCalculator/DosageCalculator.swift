//
//  DosageCalculator.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/23/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

class DosageCalculator {
    
    let concentration: Int
    let weight: Int
    let frequency: DosageFrequency
    
    var result: Int {
        return concentration * weight / frequency.times
    }
    
    init(concentration: Int, weight: Int, frequency: DosageFrequency) {
        self.concentration = concentration
        self.weight = weight
        self.frequency = frequency
    }
}

