//
//  Presentation.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/23/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

class Presentation
{
    var title: String =  ""
    var subtitle: String = ""
    var description: String = ""
    var ean: String = ""
    
    var type: PresentationType?
    
    var descriptionText: String {
        return "\(title), \(subtitle)"
    }
    
    init(ean: String, title: String, subtitle: String, description: String) {
        self.ean = ean
        self.title = title
        self.subtitle = subtitle
        self.description = description
        self.type = calculateTypeByDescription(text: description)
    }
    
    func prescriptionText(with dosage: Int, length: Int, frequency: DosageFrequency) -> String {
        let posology = "\(type!.descriptionForDosage(dosage: dosage))"
        let treatmentLength = "\(length) dia\(length > 0 ? "s" : "")"
        
        return "\(posology) de \(frequency.rangeText) por \(treatmentLength)."
    }

    private func calculateTypeByDescription(text: String) -> PresentationType? {
        //(.*) ([0-9]*)mg - Amoxicilina 500mg
        //(.*) ([0-9]*)mg\/mL - Amoxicilina 50mg/mL
        //(.*) ([0-9]*)mg\/(.*)mL - Amoxicilina 125mg/5mL
        
        let matchXXMgYYmL = text.matches(for: "(.*) ([0-9]*)mg/(.*)mL$")
        if matchXXMgYYmL.count == 3 {
            if let mgs = Int(matchXXMgYYmL[1]), let mls =  Int(matchXXMgYYmL[2]) {
                let quantity = mgs
                return PresentationType.Liquid(quantity: quantity, ml: mls)
            }
        }
        
        let matchXXMgmL = text.matches(for: "(.*) ([0-9]*)mg/mL$")
        if matchXXMgmL.count == 2 {
            if let quantity = Int(matchXXMgmL[1]) {
                return PresentationType.Liquid(quantity: quantity, ml: 1)
            }
        }
        
        let matchMg = text.matches(for: "(.*) ([0-9]*)mg$")
        if matchMg.count == 2 {
            if let quantity = Int(matchMg[1]) {
                return .Pill(quantity: quantity)
            }
        }
        
        return nil
    }
}
