//
//  ViewController.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/21/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import UIKit

class DosageCalculatorViewController: UIViewController
{

    @IBOutlet weak var weight: UITextField!
    
    @IBOutlet weak var concentrationText: UILabel!
    @IBOutlet weak var minConcentration: UILabel!
    @IBOutlet weak var maxConcentration: UILabel!
    @IBOutlet weak var concentrationSlider: UISlider!
    
    @IBOutlet weak var frequencySelector: UISegmentedControl!
    
    @IBOutlet weak var durationText: UILabel!
    @IBOutlet weak var minDuration: UILabel!
    @IBOutlet weak var maxDuration: UILabel!
    @IBOutlet weak var durationSlider: UISlider!
    
    @IBOutlet weak var dosageResultText: UILabel!
    
    @IBOutlet weak var dosageInfoBackgroundView: UIView!
    
    typealias Model = DosageCalculatorViewModel
    
    private var model = Model.initial
    private var repository = PresentationRepository();
    private var actual: Presentation?
    
    private var embededController: PosologyListViewController?
    
    func setModel(model: Model) {
        self.model = model
    }
    
    func setRepository(repository: PresentationRepository) {
        self.repository = repository
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController!.delegate = self

        bindModelUpdateToUI()
        forceUIUpdate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sliderConcentrationValueChanged(sender: UISlider) {
        let selectedValue = Int(sender.value)
        model.concentration = selectedValue
    }

    @IBAction func sliderDurationValueChanged(sender: UISlider) {
        let selectedValue = Int(sender.value)
        model.length = selectedValue
    }

    @IBAction func segmentFrequencyValueChanged(_ sender: UISegmentedControl) {
        if let frequency = DosageFrequency.frequencyBy(index: sender.selectedSegmentIndex) {
            model.frequency = frequency
        }
    }
    
    @IBAction func textFieldWeightValueChanged(_ sender: UITextField) {
        if let weightText = sender.text {
            if let newValue = Int(weightText) {
                model.weight = newValue
            } else {
                sender.text = "\(model.weight)"
            }
        } else {
            sender.text = "\(model.weight)"
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        weight.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier! {
        case "embedPosology":
            if let embedViewController = segue.destinationViewController as? PosologyListViewController {
                embededController = embedViewController
            }
            
            case "posologyDetail":
                guard actual != nil else { return }
                if let detailViewController = segue.destinationViewController as? PosologyListViewController {
                detailViewController.setModel(singlePosology: false,
                                            substance:model.substance,
                                            dosage: model.dosage,
                                            length: model.length,
                                            frequency: model.frequency,
                                            actual: actual!,
                                            presentations: repository.presentations)
                    }
        default:
            print("Unknown segue: \(segue.identifier)")
        }

    }
}

extension DosageCalculatorViewController // UI binding and update
{
    private func bindModelUpdateToUI() {
        model.dosageDidChange = {
            self.updateUI(newModel: $0)
        }
    }
    
    private func forceUIUpdate() {
        model.update(true)
    }

    private func updateUI(newModel: Model) {
        self.title = newModel.substance
        self.weight.text = newModel.weight.description
        
        self.concentrationText.text = "\(newModel.concentration)mg/kg/dia"
        self.minConcentration.text = newModel.concentrationRange.lowerBound.description
        self.maxConcentration.text = newModel.concentrationRange.upperBound.advanced(by: -1).description
        self.concentrationSlider.minimumValue = Float(newModel.concentrationRange.lowerBound)
        self.concentrationSlider.maximumValue = Float(newModel.concentrationRange.upperBound.advanced(by: -1))
        self.concentrationSlider.value = Float(newModel.concentration)
        
        self.durationText.text = "\(newModel.length) \(newModel.length == 1 ? "dia" : "dias")"
        self.minDuration.text = newModel.lengthRange.lowerBound.description
        self.maxDuration.text = newModel.lengthRange.upperBound.advanced(by: -1).description
        self.durationSlider.minimumValue = Float(newModel.lengthRange.lowerBound)
        self.durationSlider.maximumValue = Float(newModel.lengthRange.upperBound.advanced(by: -1))
        self.durationSlider.value = Float(newModel.length)
        
        self.frequencySelector.setTitle(DosageFrequency.frequency6by6.textRepresentation, forSegmentAt: 0)
        self.frequencySelector.setTitle(DosageFrequency.frequency8by8.textRepresentation, forSegmentAt: 1)
        self.frequencySelector.setTitle(DosageFrequency.frequency12by12.textRepresentation, forSegmentAt: 2)
        self.frequencySelector.selectedSegmentIndex = newModel.frequency.index
        
        self.dosageResultText.text = "\(newModel.dosage)mg por dose"
        
        if let presentation = repository.findBy(dosage: newModel.dosage, substance: newModel.substance) {
            actual = presentation
            updateEmbedController()
        }
    }

    private func updateEmbedController() {
        if let actual = actual {
            embededController?.setModel(singlePosology: true,
                                        substance:model.substance,
                                        dosage: model.dosage,
                                        length: model.length,
                                        frequency: model.frequency,
                                        actual: actual,
                                        presentations: [actual])
        }
    }
}

extension DosageCalculatorViewController: UINavigationControllerDelegate
{

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = item
        
        if viewController is PosologyListViewController {
            viewController.title = "Selecione uma posologia"
        } else {
            viewController.title = model.substance
        }
    }
    
}
