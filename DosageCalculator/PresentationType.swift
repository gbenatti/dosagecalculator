//
//  PresentationType.swift
//  DosageCalculator
//
//  Created by Georges Benatti Jr on 6/23/16.
//  Copyright © 2016 Georges Benatti Jr. All rights reserved.
//

import Foundation

enum PresentationType
{
    case Pill(quantity: Int)
    case Liquid(quantity: Int, ml: Int)
    
    func descriptionForDosage(dosage: Int) -> String {
        let count = countForDosage(dosage: dosage)

        switch self {
        case Pill:
            return "\(count) comprimido\(count > 0 ? "s" : "") via oral"
        case Liquid:
            return "\(count)ml via oral"
        }
    }

    static func bestPresentatiomForDosage(p1: PresentationType, p2: PresentationType, dosage: Int) -> Bool {
        let nearDosage1 = p1.nearestDosage(original:dosage)
        let nearDosage2 = p2.nearestDosage(original:dosage)
        
        let diff1 = abs(nearDosage1 - dosage)
        let diff2 = abs(nearDosage2 - dosage)
        
        if diff1 == diff2 {
            return p1.comparePillAndLiquid(other: p2)
        } else {
            return diff1 < diff2
        }
    }

    
}

extension PresentationType
{
    private func countForDosage(dosage: Int) -> Int {
        let realDosage = nearestDosage(original: dosage)
        
        switch self {
        case let Pill(valueInMg):
            return realDosage/valueInMg
        case let Liquid(valueInMg, ml):
            return (realDosage/valueInMg) * ml
        }
    }
    
    private func nearestDosage(original dosage: Int) -> Int {
        switch self {
        case let Pill(valueInMg):
            let count = Float(dosage)/Float(valueInMg)
            return Int(round(count) * Float(valueInMg))
        case let Liquid(valueInMg, _):
            let count = Float(dosage)/Float(valueInMg)
            return Int(round(count) * Float(valueInMg))
        }
    }
    
    private func comparePillAndLiquid(other: PresentationType) -> Bool {
        switch self {
        case Liquid:
            switch other {
            case Pill:
                return false
            case Liquid:
                return true
            }
        case Pill:
            return true
        }
    }
}
